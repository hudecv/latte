<?php

declare(strict_types=1);

namespace BlamelessWeb;

use Nette;

class LatteExtension extends Nette\DI\CompilerExtension
{
    private $defaults = [];

    public function loadConfiguration()
    {
        $this->compiler->loadDefinitionsFromConfig(
            $this->loadFromFile(__DIR__ . '/config/latte.neon')['services']
        );
    }
}