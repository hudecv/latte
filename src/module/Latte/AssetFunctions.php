<?php

declare(strict_types=1);

namespace BlamelessWeb\MyLatte;

use Latte;

final class AssetFunctions extends LatteFunctions
{
	public static function asset($type, $url)
	{
		$latte = new Latte\Engine;
		return $latte->render(__dir__ . '/templates/asset.latte', [
			'type' => $type,
			'url' => Filters::mtime($url)
		]);
	}
}