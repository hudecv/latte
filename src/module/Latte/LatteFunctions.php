<?php

declare(strict_types=1);

namespace BlamelessWeb\MyLatte;

class LatteFunctions
{
	public static function getAssetPath($url): string {
		return self::getPath($url, 'assets');
	}

	public static function getPath($url, $folder = null): string {
		$dir = __dir__;
		$dir = substr($dir, 0, strpos($dir, "/vendor"));
		return $dir . '/www/' . (is_null($folder) ? $url : $folder . '/' . $url);
	}

	public static function getFileMTime($url): int {
		$paths = [
			self::getAssetPath($url),
			self::getPath($url)
		];

		foreach ($paths as $path) {
			if (is_file($path)) {
			    return filemtime($path);
			}	
		}
		return 0;
	}

    public function getFunctions() {
    	$class = get_class($this);
	    $array1 = get_class_methods($class);
	    if($parent_class = get_parent_class($class)){
	        $array2 = get_class_methods($parent_class);
	        $array3 = array_diff($array1, $array2);
	    }else{
	        $array3 = $array1;
	    }
	    return($array3);
    }
}