<?php

declare(strict_types=1);

namespace BlamelessWeb\MyLatte;

final class Filters
{
	const MONTHS_CS = ["Ledna", "Února", "Března", "Dubna", "Května", "Června", "Července", "Srpna", "Září", "Října", "Listopadu", "Prosince"];
	const MONTHS_EN = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

	const STRIPPABLE = "<>?!+|\"&";
	const NOCHAR = "";
	const WHITESPACE = " ";
	const DASH = "-";

	const DIACRITICS = ['ä'=>'a','Ä'=>'A','á'=>'a','Á'=>'A','à'=>'a','À'=>'A','ã'=>'a','Ã'=>'A','â'=>'a','Â'=>'A','č'=>'c','Č'=>'C','ć'=>'c','Ć'=>'C','ď'=>'d','Ď'=>'D','ě'=>'e','Ě'=>'E','é'=>'e','É'=>'E','ë'=>'e','Ë'=>'E','è'=>'e','È'=>'E','ê'=>'e','Ê'=>'E','í'=>'i','Í'=>'I','ï'=>'i','Ï'=>'I','ì'=>'i','Ì'=>'I','î'=>'i','Î'=>'I','ľ'=>'l','Ľ'=>'L','ĺ'=>'l','Ĺ'=>'L','ń'=>'n','Ń'=>'N','ň'=>'n','Ň'=>'N','ñ'=>'n','Ñ'=>'N','ó'=>'o','Ó'=>'O','ö'=>'o','Ö'=>'O','ô'=>'o','Ô'=>'O','ò'=>'o','Ò'=>'O','õ'=>'o','Õ'=>'O','ő'=>'o','Ő'=>'O','ř'=>'r','Ř'=>'R','ŕ'=>'r','Ŕ'=>'R','š'=>'s','Š'=>'S','ś'=>'s','Ś'=>'S','ť'=>'t','Ť'=>'T','ú'=>'u','Ú'=>'U','ů'=>'u','Ů'=>'U','ü'=>'u','Ü'=>'U','ù'=>'u','Ù'=>'U','ũ'=>'u','Ũ'=>'U','û'=>'u','Û'=>'U','ý'=>'y','Ý'=>'Y','ž'=>'z','Ž'=>'Z','ź'=>'z','Ź'=>'Z'];

	public static function date_cs($date)
	{
		if (!$date) {
			return "";
		}
		return str_replace(self::MONTHS_EN, self::MONTHS_CS, $date);
	}

	public static function normalize($string)
	{
		$string = str_replace(' ', ' ', $string);
		$string = str_replace('.', '', $string);
		$string = mb_strtolower(self::stripAccents($string));
		$string = str_replace(str_split(self::STRIPPABLE), self::NOCHAR, $string);
		$string = implode(self::DASH, array_filter(explode(self::WHITESPACE, $string)));

		return $string;
	}

	public static function mtime($url)
	{
		return $url . "?" . LatteFunctions::getFileMTime($url);
	}

	public static function loader(string $filter)
    {
        return (method_exists(__CLASS__, $filter) ? call_user_func_array([__CLASS__, $filter], array_slice(func_get_args(), 1)) : NULL);
    }

    private static function stripAccents($str) {
	    return strtr($str, self::DIACRITICS);
	}
}